const path = require('path')

module.exports = {
  entry: {
    app: path.resolve(__dirname, './assets/js/app.js')
  },
  output: {
    path: path.resolve(__dirname, './assets/dist'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: ['@babel/plugin-proposal-object-rest-spread']
          }
        }
      }
    ]
  }
}