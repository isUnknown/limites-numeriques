import Store from "../../store.js";

const BtnsEdit = {
  props: ["block"],
  data: function () {
    return {
      store: Store,
      initialText: this.block.content.text,
    };
  },
  computed: {
    isEditMode: function () {
      return (
        this.block.content.isedit === true ||
        this.block.content.isedit === "true"
      );
    },
    isMultiselect: function () {
      return (
        this.store.state.blocks.localVersion.filter((block) => block.isSelected)
          .length > 1
      );
    },
  },
  template: `
    <div 
      v-if="!isMultiselect"
      id="edit-buttons--left" 
      class="| flex prevent-unselect"
      :style="
        'z-index: 999'
      "
    >
        <button 
            v-if="isEditMode && block.type !== 'image'"
            id="valid" 
            class="| border border--blue prevent-unselect"
            title="Bold"
            @click="valid"
        >
          valider
          <span class="sr-only">valider</span>
        </button>
        <button 
          v-if="block.type !== 'representative'"
          id="remove" 
          class="| border border--blue prevent-unselect"
          title="Bold"
          @click="remove"
        >
          supprimer
          <span class="sr-only">supprimer</span>
        </button>
        <button 
            v-if="isEditMode && block.type !== 'image'"
            id="cancel" 
            class="| border border--blue prevent-unselect"
            title="Bold"
            @click="cancel"
        >
          annuler
          <span class="sr-only">annuler</span>
        </button>
        <button 
            v-if="(block.type === 'text' || block.type === 'markdown') && !isEditMode && block.type !== 'image'"
            id="edit" 
            class="| border border--blue prevent-unselect"
            title="Bold"
            @click="edit"
        >
          éditer
          <span class="sr-only">éditer</span>
        </button>
    </div>
  `,
  methods: {
    valid: function () {
      this.block.content.isedit = false;
      this.store.state.isUpToDate = false;
      this.store.backupBlocks();
    },
    remove: function () {
      this.store.removeBlockById(this.block.id);
      this.store.state.isUpToDate = false;
    },
    cancel: function () {
      if (this.block.content.text.length === 0) {
        this.remove();
      } else {
        this.block.content.text = this.initialText;
        this.block.content.isedit = false;
      }
    },
    edit: function () {
      this.store.clearSelection();
      this.store.enableEditBlockById(this.block.id);
    },
    enableShortkeys: function () {
      document.addEventListener("keydown", (event) => {
        if (event.key === "Escape") {
          this.cancel();
        }
        if ((event.ctrlKey || event.metaKey) && event.key === "Enter") {
          this.valid();
        }
        if ((event.ctrlKey || event.metaKey) && event.key === "Backspace") {
          this.remove();
        }

        if (
          (this.block.isedit !== true || this.block.isedit !== "true") &&
          this.block.isSelected &&
          event.key === "Backspace"
        ) {
          this.remove();
        }
      });

      document.addEventListener("click", (event) => {
        if (event.target.textContent === "texte") return;

        if (this.block.content.text.length === 0) {
          this.remove();
        } else if (
          event.target.nodeName === "HTML" ||
          event.target.id === "app" ||
          !event.target.closest(".prevent-unselect")
        ) {
          this.store.disableAllEdit();
        }
      });
    },
  },
  mounted: function () {
    this.enableShortkeys();
  },
};

export default BtnsEdit;
