import Store from "../../store.js"
import noteMixin from "../blocks/note-mixin.js"

const BtnsMd = {
  props: {
    block: Object
  },
  mixins: [noteMixin],
  data: function () {
    return {
      store: Store
    }
  },
  template: `
    <div id="md-buttons" class="| flex prevent-unselect">
        <button 
            id="bold" 
            class="btn btn--icon border border--blue"
            title="Bold" 
            @click="insert('bold')"
        >   
            B
            <span class="sr-only">Bold</span>
        </button>
        <button 
            id="italic" 
            class="btn btn--icon border border--blue"
            title="Italic" 
            @click="insert('italic')"
        >   
            I
            <span class="sr-only">Italic</span>
        </button>
        <button 
            id="code" 
            class="btn btn--icon border border--blue"
            title="Code" 
            @click="insert('code')"
        >   
          &lt;/&gt;
            <span class="sr-only">Code</span>
        </button>
        <button 
            id="link" 
            class="btn btn--icon border border--blue"
            title="Lien" 
            @click="insert('link')"
        >   
            <svg class="icon"><use xlink:href="#icon-link"></use></svg>
            <span class="sr-only">Lien</span>
        </button>
        <button 
            id="quote" 
            class="btn btn--icon border border--blue"
            title="Citation" 
            @click="insert('quote')"
        >   
            <svg class="icon"><use xlink:href="#icon-quote"></use></svg>
            <span class="sr-only">Citation</span>
        </button>
        <button 
            id="note" 
            class="btn btn--icon border border--blue"
            title="Note" 
            @click="insert('note')"
        >   
            <svg class="icon"><use xlink:href="#icon-note"></use></svg>
            <span class="sr-only">Note</span>
        </button>
        <a 
            id="markdown" 
            class="btn btn--icon border border--blue"
            title="Aide markdown"
            href="https://duckduckgo.com/?t=ffab&q=markdown+cheatsheet&atb=v243-1&ia=answer&iax=1"
            target="_blank"
        >   
            <svg class="icon"><use xlink:href="#icon-markdown"></use></svg>
            <span class="sr-only">Aide markdown</span>
        </a>
    </div>
  `,
  methods: {
    preventDeselect: function () {
      document.querySelectorAll('#md-buttons .btn').forEach(btn => {
        btn.addEventListener('mousedown', e => {
          e = e || window.event
          e.preventDefault()
        })
      })
    },
    insert: function (btn) {
      let field = document.querySelector('textarea')
      let syntax
      let selectionStart

      switch (btn) {
        case 'bold':
          syntax = `****`
          selectionStart = 2
          break;
        case 'italic':
          syntax = `__`
          selectionStart = 1
          break;
        case 'link':
          syntax = `[](https://)`
          selectionStart = 1
          break;
        case 'code':
          syntax = `\`\``
          selectionStart = 1
          break;
        case 'quote':
          syntax = `> `
          selectionStart = 2
          break;
        case 'note':
          const container = document.querySelector('.blocks')
          const fieldWidth = parseInt(window.getComputedStyle(field).getPropertyValue('width'))
          const fieldX = this.getOffset(field).left
          const fieldY = this.getOffset(field).top
          const containerX = this.getOffset(container).left
          const containerY = this.getOffset(container).top
          const gridGap = 10
          const noteTranslateX = (fieldX - containerX) + (fieldWidth + gridGap)
          const noteTranslateY = (fieldY - containerY)
          const noteId = Date.now().toString()

          syntax = '[]'
          selectionStart = 1
          const newBlock = {
            type: 'text',
            ref: noteId,
            id: this.block.id,
            left: noteTranslateX,
            top: noteTranslateY,
            text: `[${this.noteIndex}]`
          }
          this.store.addBlock(newBlock.type, newBlock.ref, newBlock.id, newBlock.left, newBlock.top, newBlock.text)
          this.addRef(newBlock.ref)
          break;
      }

      // Current Selection
      const textarea = document.querySelector('textarea')
      const currentSelectionStart = textarea.selectionStart;
      const currentSelectionEnd = textarea.selectionEnd;
      const currentText = textarea.value;
      const placeholder = btn === 'note' ? this.noteIndex : 'écrivez ici'
      const selectionEnd = btn === 'note' ? selectionStart + this.noteIndex.toString.length : selectionStart + (placeholder.length)
    
      if( currentSelectionStart === currentSelectionEnd ) {
        const textWithSyntax = textarea.value = currentText.substring( 0, currentSelectionStart ) + syntax + currentText.substring( currentSelectionEnd );
        textarea.value = textWithSyntax.substring( 0, currentSelectionStart + selectionStart ) + placeholder + textWithSyntax.substring( currentSelectionStart + selectionStart )
    
        textarea.focus();
        textarea.setSelectionRange(currentSelectionStart + selectionStart, currentSelectionEnd + selectionEnd);
      } else {
        const selectedText = currentText.substring( currentSelectionStart, currentSelectionEnd );
        const withoutSelection = currentText.substring( 0, currentSelectionStart ) + currentText.substring( currentSelectionEnd );
        const textWithSyntax = withoutSelection.substring( 0, currentSelectionStart ) + syntax + withoutSelection.substring( currentSelectionStart );
    
        // Surround selected text
        textarea.value = textWithSyntax.substring( 0, currentSelectionStart + selectionStart ) + selectedText + textWithSyntax.substring( currentSelectionStart + selectionStart );
    
        textarea.focus();
        textarea.selectionEnd = currentSelectionEnd + selectionStart + selectedText.length;
      }
      // Update block.content.text
      setTimeout(() => {
        console.log(textarea.value)
        this.block.content.text = textarea.value
      }, 100);

    },
    addRef: function (id) {
      this.block.content.refs += this.block.content.refs.length === 0 ? id : `, ${id}`
    },
    getSelectedText: function () {
      const textField = document.querySelector('textarea')
      const selectedText = textField.value.substring(textField.selectionStart, textField.selectionEnd)
      return selectedText
    },
    getOffset: function (el) {
      const rect = el.getBoundingClientRect();
      return {
        left: rect.left + window.scrollX,
        top: rect.top + window.scrollY
      }
    }
  },
  mounted: function () {
    this.preventDeselect()
  }
}

export default BtnsMd
