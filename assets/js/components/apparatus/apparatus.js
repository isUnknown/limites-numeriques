import Index from "./index/index.js";
import PrevNext from "./tune/composition/prev-next.js";
import Store from "../../store.js";
import Register from "./register.js";
import Tune from "./tune/tune.js";
import Modify from "./modify/modify.js";
import CurrentSection from "./current-section.js";

const apparatus = {
  props: {
    index: Array,
    tags: Array,
    page: String,
    pageUri: String,
    "page-colors-label": String,
    breadcrumb: Array,
    isLog: Boolean,
  },
  components: {
    index: Index,
    tune: Tune,
    register: Register,
    modify: Modify,
    "prev-next": PrevNext,
    "current-section": CurrentSection,
  },
  data: function () {
    return {
      store: Store,
      offscreenBlocks: {
        top: null,
        right: null,
        bottom: null,
        left: null,
      },
    };
  },
  template: `
    <div 
      id="apparatus" 
      ref="apparatus" 
      :class="{ active: store.state.apparatus.isActive }"
    >
      <div 
        id="apparatus__top"
        class="apparatus__section"
      >
        <index
          class="apparatus__btn"
          :index="index"
          :page="page"
          :breadcrumb="breadcrumb"
          @switchOffApparatus="off"
        ></index>
        <tune
          class="apparatus__btn"
          :tags="tags"
          position="top right"
          @switchOffApparatus="off"
        ></tune>
      </div>

      <div 
        id="apparatus__top--mobile"
        class="apparatus__section apparatus__section--mobile"
      >
        <!-- <modify
          class="apparatus__btn"
          @click="changeCurrentSection('modifier')"
          @switchOffApparatus="off"
        ></modify> -->
      </div>

      <div id="apparatus__middle">
        <prev-next v-if="store.state.layout === 'full'"></prev-next>
      </div>
     
      <div
        id="apparatus__bottom"
        class="apparatus__section"
      >
        <div></div>
        <register v-if="!store.state.isUpToDate && (store.state.isLog || store.state.isOpenSpace)"
          class="apparatus__btn"
          :page="page"
          :pageUri="pageUri"
          :page-colors-label="pageColorsLabel"
        ></register>
        <modify
          v-if="store.config.unlogEdition || isLog"
          @switchOffApparatus="off"
          class="apparatus__btn"
        ></modify>
      </div>
      
      <div 
        id="apparatus__bottom--mobile"
        class="apparatus__section apparatus__section--mobile"
      >
        <div id="apparatus__bottom__options">
          <index
            class="apparatus__btn"
            :index="index"
            :page="page"
            :breadcrumb="breadcrumb"
            @switchOffApparatus="off"
          ></index>
          <!-- <register v-if="!store.state.isUpToDate && (store.state.isLog || store.state.isOpenSpace)"
            class="apparatus__btn"
            :page="page"
            :page-colors-label="pageColorsLabel"
          ></register>
          <tune
            class="apparatus__btn"
            :tags="tags"
            @switchOffApparatus="off"
          ></tune> -->
        </div>

        <current-section
          v-if="store.state.apparatus.isActive"
          @off="off"
        ></current-section>
      </div>
    </div>
  `,
  methods: {
    off: function (e) {
      this.clearActiveItems();
      this.store.state.apparatus.isActive = false;
      this.store.state.apparatus.currentSection = "";
      this.store.state.apparatus.currentMenu = "";
      const lastBlock = this.store.getLastBlock();
      if (lastBlock.type === "text" && lastBlock.content.text.length !== 0)
        this.store.disableAllEdit();
      if (!e) return;
    },
    clearActiveItems: function () {
      const activeItems = document.querySelectorAll(".active-section");
      activeItems.forEach((activeItem) => {
        activeItem.classList.remove("active-section");
      });
    },
    enableEscHotkey: function () {
      document.addEventListener("keyup", (e) => {
        if (e.keyCode === 27) {
          this.off(e);
        }
      });
    },
    enableDetectOffscreenBlocks: function () {
      setTimeout(() => {
        this.setOffscreenBlocks();
      }, 500);
    },
    setOffscreenBlocks: function () {
      this.offscreenBlocks.right =
        this.isOffscreenY() === undefined ? false : true;
      this.offscreenBlocks.bottom =
        this.isOffscreenX() === undefined ? false : true;
    },
    getXPosFromTransform: function (transform) {
      const x = transform.match(/\((.*)\)/)[1].split(",")[0];
      return parseInt(x);
    },
    isOffscreenY: function () {
      return this.store.state.blocks.localVersion.find((block) => {
        const y = block.content.transform.match(/\((.*)\)/)[1].split(",")[1];
        return parseInt(y) > window.scrollX;
      });
    },
    isOffscreenX: function () {
      return this.store.state.blocks.localVersion.find((block) => {
        const x = block.content.transform.match(/\((.*)\)/)[1].split(",")[0];
        return this.getXPosFromTransform(parseInt(x)) > window.scrollX;
      });
    },
  },
  mounted: function () {
    document.querySelector("#app").addEventListener("click", (e) => {
      const tagName = e.target.tagName.toLowerCase();
      const isNotException =
        tagName !== "label" &&
        tagName !== "input" &&
        tagName !== "button" &&
        tagName !== "textarea" &&
        !e.target.classList.contains("menu__arrow") &&
        tagName !== "a" &&
        e.target.closest("button") === null;

      if (isNotException) {
        this.off(e);
      }
    });
    this.enableEscHotkey();
    this.setOffscreenBlocks();
  },
};

export default apparatus;
