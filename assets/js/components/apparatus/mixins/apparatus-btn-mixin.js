import Store from "../../../store.js"

const apparatusBtnMixin = {
  data: function() {
    return {
      store: Store
    }
  },
  computed: {
    isApparatusActive: function() {
      return this.store.state.apparatus.isActive
    }
  },
  watch: {
    isApparatusActive: function(newValue) {
      console.log('apparatusChange', newValue)
      if (newValue === false) {
        this.isOpen = false
      }
    }
  },
  methods: {
    toggleIsOpen: function(menuName) {
      if (this.store.state.apparatus.isActive) {
        this.$emit('switchOffApparatus')
      } else {
        this.isOpen = !this.isOpen
        this.store.state.apparatus.isActive = this.isOpen
        if (this.isOpen) {
          this.store.state.apparatus.currentSection = menuName
          this.store.state.apparatus.currentMenu = menuName
        }
      }
    }
  }
}

export default apparatusBtnMixin