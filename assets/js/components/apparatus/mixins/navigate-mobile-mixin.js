const navigateMobileMixin = {
  methods: {
    activeParents: function(activeSection) {
      const parent = activeSection.tagName === 'UL' ? activeSection.closest('li') : activeSection.closest('ul')
      if (!parent.classList.contains('active-section')) parent.classList.add('active-section')
      
      if (parent.classList.contains('lock-visible')) return
      this.activeParents(parent)
    },
    navigateTo: function(section, event) {
      if (this.store.state.isDesktop) return
      event.preventDefault()
      
      this.store.state.apparatus.currentSection = section

      const newActiveSection = event.currentTarget.closest('li')
      newActiveSection.classList.add('active-section')

      this.activeParents(newActiveSection)
    }
  }
}

export default navigateMobileMixin