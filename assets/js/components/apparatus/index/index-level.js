import Store from "../../../store.js";
import navigateMobileMixin from "../mixins/navigate-mobile-mixin.js";

const IndexLevel = {
  name: "index-level",
  props: {
    "index-level": Array,
    "parent-url": String,
    breadcrumb: Array,
    page: String,
    "parent-name": String,
  },
  mixins: [navigateMobileMixin],
  data: function () {
    return {
      isNextLevelDisplayed: false,
      store: Store,
    };
  },
  computed: {
    isCurrent: function () {
      return this.parentName === this.page;
    },
    containsActiveCategory: function () {
      const containsActiveCategory = this.indexLevel.some((category) => {
        return this.breadcrumb.includes(category.title.toLowerCase());
      });
      return containsActiveCategory;
    },
  },
  template: `
    <ul 
      :class="{
        'lock-visible': containsActiveCategory && store.state.apparatus.isActive
      }"
    >
      <li v-if="parentUrl.length > 0">
        <span>
          <button 
            class="text-2 border"
            :class="{'lock-active': isCurrent}"
          >
            <a :href="parentUrl">tout</a>
          </button>
        </span>
      </li>
     
      <li
        v-for="(category, childIndex) in indexLevel"
      >
        <span>
          <button 
            class="text-2 border"
            :class="isInBreadcrumb(category.title)"
            tabindex="-1"
          >
            <a 
              :href="category.url"
              :title="'Aller à la page ' + category.title"
            >{{ category.title }}</a>
          </button>
          
          <span 
            v-if="category.children"
            class="menu__arrow | text-2 border"
            tabindex="-1"
          ><a 
            @click="navigateTo(category.title, $event)"
            :href="category.url"
            :title="'Aller à la page ' + category.title"
          >→</a></span>
        </span>
        <index-level
          v-if="category.children"
          :index-level="category.children"
          :parent-url="category.url"
          :parent-name="category.title"
          :breadcrumb="breadcrumb"
          :page="page"
        ></index-level>
      </li>
    </ul>
  `,
  methods: {
    isInBreadcrumb: function (category) {
      if (category.toLowerCase() === "accueil") return false;
      if (this.breadcrumb.includes(category.toLowerCase())) {
        return "lock-active";
      }
    },
  },
};

export default IndexLevel;
