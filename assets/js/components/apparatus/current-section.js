import Store from "../../store.js";

const CurrentSection = {
  data: function () {
    return {
      store: Store,
      previousBtn: false,
    };
  },
  computed: {
    currentSection: function () {
      return this.store.state.apparatus.currentSection;
    },
  },
  watch: {
    currentSection: function () {
      const activeMenu = document
        .querySelector(".lock-visible")
        .parentNode.querySelector("span > button").textContent;
      this.previousBtn = this.currentSection !== activeMenu;
    },
  },
  template: `
    <div id="apparatus--mobile__current-section">
      <button @click="previous" class="no-hover" :class="{unvisible: !previousBtn}">←</button>
      <span id="current-section__title" class="text-2">{{ store.state.apparatus.currentSection }}</span>
      <button @click="close" class="no-hover"><img id="apparatus--mobile__current-section__close-image" src="/assets/svg/close.svg" /></button>
    </div>
  `,
  methods: {
    previous: function () {
      const activeSections = document.querySelectorAll(".active-section");
      const currentSectionLi = activeSections[activeSections.length - 1];
      const currentSectionUl = activeSections[activeSections.length - 2];

      currentSectionLi.classList.remove("active-section");
      currentSectionUl.classList.remove("active-section");

      const newActiveSections = document.querySelectorAll(".active-section");

      if (newActiveSections.length === 0) {
        this.store.state.apparatus.currentSection =
          this.store.state.apparatus.currentMenu;
      } else {
        this.store.state.apparatus.currentSection =
          newActiveSections[newActiveSections.length - 1].querySelector(
            "span > button"
          ).textContent;
      }
    },
    close: function () {
      this.$emit("off");
    },
  },
};

export default CurrentSection;
