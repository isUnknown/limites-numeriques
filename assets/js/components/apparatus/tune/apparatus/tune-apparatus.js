const TuneApparatus = {
  template: `
    <li>
      <span>
        <button 
          class="text-2 border"
          tabindex="-1"
        >
          interface
        </button>
        
        <span 
          class="menu__arrow | text-2 border"
          tabindex="-1"
        >→</span>
      </span>
      <ul>
        <li>
          <button class="border text-2" @click="toggleGrid">
            grille
          </button>
        </li>
      </ul>
    </li>
  `,
  methods: {
    toggleGrid: function() {
      document.querySelector('#app').classList.toggle('grid')
    }
  }
}

export default TuneApparatus