import Store from "../../../../store.js";

const Composition = {
  props: {
    positions: String,
  },
  data: function () {
    return {
      store: Store,
      isOpen: false,
    };
  },
  computed: {
    isCurrentEditBlock: function () {
      return this.store.state.blocks.localVersion.some(
        (block) => block.content.isedit == true
      );
    },
  },
  template: `
    <li>
      <span>
        <button 
          class="text-2 border"
          tabindex="-1"
        >
          composition
        </button>
        
        <span 
          class="menu__arrow | text-2 border"
          tabindex="-1"
        >{{  positions.isLeft ? '→': '←' }}</span>
      </span>
      
        <form action="">
          <ul>
            <li v-for="layout in store.state.layouts" @click="switchOffApparatus">
              <span>
                <label :class="store.state.layout === layout.value ? 'active': false" class="| border text-2" :for="layout.value">{{ layout.label }}</label>
                <input 
                  type="radio" 
                  v-model="store.state.layout"
                  name="composition" 
                  :id="layout.value" 
                  :value="layout.value" 
                  @change="store.changeLayout(layout.value)"
                >
              </span>
            </li>
          </ul>
        </form>
      
    </li>
  `,
  methods: {
    switchLayout: function (direction) {
      const activeValue = document.querySelector(
        ".inputWrapper.active input"
      ).value;
      this.layouts.forEach((layout) => {
        layout.active = false;
        if (layout.value === activeValue) {
          const layoutIndex = this.layouts.indexOf(layout);
          const indexBoundary =
            direction === "next" ? this.layouts.length - 1 : 0;
          if (layoutIndex === indexBoundary) {
            const resetValue =
              direction === "next"
                ? this.layouts[0].value
                : this.layouts[this.layouts.length - 1].value;
            this.store.changeLayout(resetValue);
          } else {
            const neighborValue =
              direction === "next"
                ? this.layouts[layoutIndex + 1].value
                : this.layouts[layoutIndex - 1].value;
            this.store.changeLayout(neighborValue);
          }
        }
      });
    },
    enableShortcut: function () {
      document.addEventListener("keyup", (event) => {
        if (this.isCurrentEditBlock) return;
        switch (event.key) {
          case "p":
            this.toggle();
            break;
          case "ArrowDown":
            if (!this.isOpen) return;
            this.switchLayout("next");
            break;
          case "ArrowRight":
            if (!this.isOpen) return;
            this.switchLayout("next");
            break;
          case "ArrowUp":
            if (!this.isOpen) return;
            this.switchLayout("previous");
            break;
          case "ArrowLeft":
            if (!this.isOpen) return;
            this.switchLayout("previous");
            break;
          case "Enter":
            if (!this.isOpen) return;
            this.close();
            break;
        }
      });
    },
    switchOffApparatus: function () {
      // CHROME BROWSER NEEDS THE TIMEOUT
      setTimeout(() => {
        this.$emit("switchOffApparatus");
      }, 10);
    },
  },
  mounted: function () {
    this.enableShortcut();
  },
};

export default Composition;
