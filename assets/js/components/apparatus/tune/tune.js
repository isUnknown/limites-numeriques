import Refine from "./refine/refine.js"
import Composition from "./composition/composition.js"
import apparatusBtnMixin from "../mixins/apparatus-btn-mixin.js"
import Store from "../../../store.js"
import TuneApparatus from "./apparatus/tune-apparatus.js"

const Tune = {
  props: {
    'tags': Array,
    'position': String
  },
  mixins: [apparatusBtnMixin],
  components: {
    'refine': Refine,
    'composition': Composition,
    'tune-apparatus': TuneApparatus
  },
  data: function() {
    return {
      isOpen: false,
      store: Store,
      name: 'régler'
    }
  },
  computed: {
    positions: function() {
      return {
        isLeft: this.position.includes('left'),
        isTop: this.position.includes('top'),
        isRight: this.position.includes('right'),
        isBottom: this.position.includes('bottom')
      }
    }
  },
  template: `
    <ul class="menu" :class="position">
      <li>
        <span @click="toggleIsOpen(name)" :class="{'hidden--mobile': store.state.apparatus.currentSection.length > 0}">
          <button 
            class="| text-2 border lock-active"
          >{{ this.name }}</button>
          <span
            class="menu__arrow | text-2 border lock-active"
            tabindex="0"
          >{{  positions.isLeft ? '→': '←' }}</span>
        </span>
        <ul v-if="isOpen" class="lock-visible--flex">
          <refine :tags="tags"></refine>
          <composition 
            positions="positions"
            @switchOffApparatus="switchOffApparatus"
          ></composition>
          <!-- <tune-apparatus></tune-apparatus> -->
        </ul>
      </li>
    </ul>
  `,
  methods: {
    switchOffApparatus: function() {
      this.$emit('switchOffApparatus')
    }
  }
}

export default Tune