import Store from "../../../store.js";

const Add = {
  data: function () {
    return {
      store: Store,
      isOpen: false,
      types: [
        {
          label: "texte",
          type: "markdown",
          open: true,
        },
        {
          label: "image",
          type: "image",
          open: false,
        },
      ],
    };
  },
  template: `
    <li>
      <span>
        <button 
          class="text-2 border"
          tabindex="-1"
        >
          ajouter
        </button>

        <span 
          class="menu__arrow | text-2 border"
          tabindex="-1"
        >←</span>
      </span>
      <ul>
        <li v-for="item in types" v-if="item.open || store.state.isLog">
          <span>
            <button class="text-2 | border" @click="create(item.type)">{{ item.label }}</button>
          </span>
        </li>
      </ul>
    </li>
  `,
  methods: {
    create: function (type) {
      const top = document.querySelector("#search").offsetHeight + 45;

      this.store.state.isUpToDate = false;

      const scrollY = window.scrollY;
      const halfViewHeight = window.innerHeight / 2 - 480 / 2;

      const scrollX = window.scrollX;
      const halfViewWidth = window.innerWidth / 2 - 585 / 2;

      const pos = {
        x: scrollX + halfViewWidth,
        y: scrollY + halfViewHeight,
      };
      this.store.addBlock(type, undefined, undefined, pos.x, pos.y, undefined);
      this.toggleIsOpen();
      this.$emit("switchOffApparatus");
      this.scrollToNewBlock();
    },
    scrollToNewBlock: function () {
      setTimeout(() => {
        const newBlock =
          document.querySelectorAll(".block")[
            document.querySelectorAll(".block").length - 1
          ];
        if (this.store.state.layout === "vertical") {
          const containerHeight =
            document.querySelector(".blocks").clientHeight;
          const newBlockHeight = newBlock.clientHeight;
          const newBlockY = containerHeight - newBlockHeight;
          window.scrollTo(0, newBlockY);
        } else if (this.store.state.layout === "horizontal") {
          const containerWidth = document.querySelector(".blocks").clientWidth;
          const newBlockWidth = newBlock.clientWidth;
          const newBlockX = containerWidth - newBlockWidth;
          window.scrollTo(newBlockX, 0);
        }
      }, 100);
    },
    toggleIsOpen: function () {
      this.isOpen = !this.isOpen;
    },
    enableShortcuts: function () {
      document.addEventListener("keydown", (event) => {
        if (event.ctrlKey || event.metaKey) {
          if (event.key === "t") {
            this.create("texte");
          }
        }
      });
    },
  },
};

export default Add;
