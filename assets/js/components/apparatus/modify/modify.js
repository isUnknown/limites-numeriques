import Store from "../../../store.js";
import apparatusBtnMixin from "../mixins/apparatus-btn-mixin.js";
import Add from "./add.js";
import navigateMobileMixin from "../mixins/navigate-mobile-mixin.js";

const Modify = {
  mixins: [apparatusBtnMixin, navigateMobileMixin],
  components: {
    add: Add,
  },
  data: function () {
    return {
      store: Store,
      isOpen: false,
    };
  },
  computed: {
    isModifications: function () {
      const allBackups = JSON.parse(localStorage.getItem("backups"));
      return allBackups[this.store.state.page].length < 1;
    },
  },
  template: `
    <ul class="menu bottom right">
      <li>
        <span @click="toggleIsOpen" :class="{'hidden--mobile': store.state.apparatus.currentSection.length > 0}">
          <button 
            class="| text-2 border lock-active"
          >éditer</button>
          <span
            class="menu__arrow | text-2 border lock-active"
            tabindex="0"
          >←</span>
        </span>
        <ul v-if="isOpen" class="lock-visible--flex">
          <add
            v-if="store.state.layout !== 'full'"
            @switchOffApparatus="switchOffApparatus"
          ></add>
          <li>
            <span>
              <button
                class="text-2 border"
                @click="reset"
              >réinitialiser l'espace</button>
              <!-- <button 
                class="text-2 border"
                @click="reset"
              > ctrl + alt + z</button> -->
            </span>
          </li>
          <!-- <li>
            <span>
              <button
                v-if="isModifications"
                class="text-2 border"
                @click="undo"
              >annuler</button>
              <button 
                class="text-2 border"
                @click="undo"
              > ctrl + z</button>
            </span>
          </li> -->
        </ul>
      </li>
    </ul>
  `,
  methods: {
    switchOffApparatus: function () {
      this.$emit("switchOffApparatus");
    },
    reset: function () {
      this.store.reset();
      this.$emit("switchOffApparatus");
    },
    undo: function () {
      this.store.undo();
      this.$emit("switchOffApparatus");
    },
  },
};

export default Modify;
