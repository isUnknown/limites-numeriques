import Store from "../../store.js";

const Register = {
  props: {
    page: String,
    pageUri: String,
    "page-colors-label": String,
  },
  data: function () {
    return {
      store: Store,
    };
  },
  template: `
    <button 
      class="| text-2 border" 
      @click="save"
      ref="register-btn"
    >enregistrer</button>
  `,
  methods: {
    save: function () {
      if (!this.store.state.isLog) return;
      this.$refs["register-btn"].textContent = "enregistrement…";
      setTimeout(() => {
        const data = {
          pageUri: this.pageUri,
          colors: this.pageColorsLabel,
          blocks: this.store.state.blocks.localVersion,
        };
        if (this.$root.debug) {
          console.log(this.$options.el + " save: ", blocks);
        }
        this.send(data);
      }, 500);
      this.store.state.blocks.serverVersion =
        this.store.state.blocks.localVersion;
    },
    send: function (data) {
      const init = {
        method: "POST",
        body: JSON.stringify(data),
      };
      fetch(`./save.json`, init)
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          if (this.$root.debug) {
            console.log(
              this.$options._componentTag + " save.json return: ",
              data
            );
          }
          this.$refs["register-btn"].textContent = "enregistré";
          this.$refs["register-btn"].classList.add("active");
          setTimeout(() => {
            this.store.state.isUpToDate = true;
          }, 1200);
        });
    },
    listenSaveShortcut: function () {
      if (this.$root.debug) {
        console.log(this.$options._componentTag + " : ");
      }
      document.addEventListener(
        "keydown",
        (e) => {
          if (
            (window.navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) &&
            e.keyCode == 83
          ) {
            e.preventDefault();
            this.save();
          }
        },
        false
      );
    },
  },
  mounted: function () {
    this.listenSaveShortcut();
  },
};

export default Register;
