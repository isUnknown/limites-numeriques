import Store from "../../store.js";
import noteMixin from "./note-mixin.js";

const BlockText = {
  props: {
    block: Object,
    isEdit: Boolean,
  },
  mixins: [noteMixin],
  data: function () {
    return {
      store: Store,
    };
  },
  computed: {
    iframeHeight: function () {
      return parseInt(this.block.content.width) / 1.5;
    },
    parsedText: function () {
      const textWithoutConfig = this.block.content.text
        .replace(/couleur-texte:(#[a-z,A-Z,0-9]+)/, "")
        .replace(/couleur-fond:(#[a-z,A-Z,0-9]+)/, "");
      const regexYoutubeUrls =
        /^(?!\()(?:https:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?.*v=)?(\w+)/gm;
      const videoConvertedText = textWithoutConfig.replaceAll(
        regexYoutubeUrls,
        `<iframe width="100%" style="height:${this.iframeHeight}px" src="https://www.youtube-nocookie.com/embed/$1" frameborder="0" allowfullscreen></iframe>`
      );
      const iframeConverted = videoConvertedText.replaceAll(
        /iframe:(.*)/gm,
        `<iframe width="100%" style="height:${this.iframeHeight}px" src="$1"></iframe>`
      );

      marked.setOptions({
        breaks: true,
        smartypants: true,
      });
      let markedText = marked.parse(iframeConverted);

      let noteIndex = 0;
      const notedMdText = markedText.replaceAll(/\[[0-9]+\]/g, (match) => {
        const html = `<a href="#${this.splitedRefs[noteIndex]}" class="note" id="${this.block.id}">${match}</a>`;
        noteIndex++;
        return html;
      });

      const isCustomTextColor = /couleur-texte:(#[a-z,A-Z,0-9]+)/.test(
        this.block.content.text
      );
      const isCustomBackgroudColor = /couleur-fond:(#[a-z,A-Z,0-9]+)/.test(
        this.block.content.text
      );
      const textColor = this.block.content.text.match(
        /couleur-texte:(#[a-z,A-Z,0-9]+)/
      )
        ? this.block.content.text.match(/couleur-texte:(#[a-z,A-Z,0-9]+)/)[1]
        : "#000";
      const backgroundColor = this.block.content.text.match(
        /couleur-fond:(#[a-z,A-Z,0-9]+)/
      )
        ? this.block.content.text.match(/couleur-fond:(#[a-z,A-Z,0-9]+)/)[1]
        : "#fff";
      const isDifferentTextColor = textColor !== this.block.content.textcolor;
      const isDifferentBackgroundColor =
        backgroundColor !== this.block.content.backgroundcolor;

      if (isDifferentTextColor || isDifferentBackgroundColor) {
        this.store.changeBlockColor(this.block.id, "text", textColor);
        this.store.changeBlockColor(
          this.block.id,
          "background",
          backgroundColor
        );
      }

      return notedMdText;
    },
    splitedRefs: function () {
      if (!this.block.content.refs.length) return [];
      return this.block.content.refs.split(", ");
    },
  },
  template: `
        <div>
            <div v-if="block.content.text.length > 0" v-html="parsedText" ref="content"></div>
            <div v-else ref="content"><p>ctrl + clic droit pour éditer.</p></div>
        </div>
    `,
  methods: {
    enableNoteHighlight: function () {
      if (this.store.state.isMobile) return;
      const notes = this.$refs.content.querySelectorAll(".note");
      notes.forEach((note, index) => {
        note.removeEventListener("mouseenter", this.highlight);
        note.removeEventListener("mouseleave", this.unhighlight);
        note.addEventListener("mouseenter", this.highlight);
        note.addEventListener("mouseleave", this.unhighlight);
      });
    },
    highlight: function (e) {
      const textContent = e.target.textContent;
      const regex = /\[([0-9]+)\]/;
      const noteIndex = parseInt(textContent.match(regex).pop());
      const refIndex = noteIndex - 1 - (this.noteFirstIndex - 1);
      const refId = this.splitedRefs[refIndex];
      this.store.highlightBlockById(this.block.id);
      this.store.highlightBlockById(refId);
    },
    unhighlight: function (e) {
      const textContent = e.target.textContent;
      const regex = /\[([0-9]+)\]/;
      const noteIndex = parseInt(textContent.match(regex).pop());
      const refIndex = noteIndex - 1 - (this.noteFirstIndex - 1);
      const refId = this.splitedRefs[refIndex];
      this.store.unhighlightBlockById(this.block.id);
      this.store.unhighlightBlockById(refId);
    },
  },
  mounted: function () {
    this.enableNoteHighlight();
  },
};

export default BlockText;
