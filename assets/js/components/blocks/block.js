import blockMixin from "./block-mixin.js";
import BlockText from "./block-text.js";
import BlockImage from "./block-image.js";
import Store from "../../store.js";
import BtnsMd from "../buttons/btns-md.js";
import blockGroup from "./block-group.js";
import BlockRelated from "./block-related.js";
import BlockRepresentative from "./block-representative.js";
import BtnsEdit from "../buttons/btns-edit.js";
import BlockMarkdown from "./block-markdown.js";

const Block = {
  mixins: [blockMixin],
  props: {
    block: Object,
    page: String,
  },
  components: {
    "block-markdown": BlockMarkdown,
    "block-image": BlockImage,
    "block-related": BlockRelated,
    "block-representative": BlockRepresentative,
    "block-group": blockGroup,
    "btns-md": BtnsMd,
    "btns-edit": BtnsEdit,
  },
  data: function () {
    return {
      store: Store,
      heightTextarea: "",
      initialZIndex: this.block.content.zindex,
      isHover: false,
    };
  },
  computed: {
    isHighlight: function () {
      return this.block.isHighlight || false;
    },
  },
  template: `
    <div 
    class="| absolute"
    :class="{front: block.isNew}"
    @contextmenu="toggleContext"
    v-if="
      block.type === 'markdown'
      && (block.content.isedit == 'true' 
      || block.content.isedit == true)"
    >
      <btns-md
        :block="block"
        :style="
            'transform: ' + block.content.transform + '; ' +
            'z-index: 999'
        "
      ></btns-md>

      <textarea 
          v-if="block.type === 'markdown' && (block.content.isedit == 'true' || block.content.isedit == true)"
          v-model="block.content.text"
          
          ref="textarea"
          :style="
              'width: ' + block.content.width + '; ' +
              'transform: ' + block.content.transform + '; '
          " 
          class="block--markdown | autoExpand border border--blue prevent-unselect" 
          placeholder="Entrez votre texte"
      />
      
      <btns-edit
        :style="
            'transform: ' + block.content.transform + ';' +
            'z-index: 999'
        "
        :block="block"
      ></btns-edit>
    </div>
    <div 
      v-else-if="store.state.isLog || !block.isHidden"
      class="block white" 
      :class="[
          'block--' + block.type,
          { edit: block.content.isEdit },
          { flex: block.type === 'options' },
          { highlight: block.isHighlight },
          { 'block--selected': block.isSelected },
          { 'block--flashing': block.isFlashing },
          { 'hidden--mobile': block.content.isdesktoponly == 'true' }
      ]" 
      :data-type="block.type" 
      :data-id="block.id" 
      :style="
          'width: ' + block.content.width + '; ' +
          'height: ' + block.content.height + '; ' +
          'transform: ' + block.content.transform + '; ' +
          'z-index: ' + block.content.zindex + ';' +
          'background-color: var(--color-light-grey);' +
          'color :' + block.content.textcolor + ';'
      "
      
      @mousedown="saveStyle"
      @mouseenter="mouseEnter"
      @mouseleave="mouseLeave"
      @contextmenu="toggleContext"
      @mouseup="click"
      
      ref="block">
      <component 
        :is="'block-' + block.type" 
        :block="block" 
        :page="page"
        :update-signal="updateSignal"
      ></component>
      <btns-edit
        v-if="block.isSelected"
        :block="block"
      ></btns-edit>
    </div>
    `,
  methods: {
    getScrollHeight: function (el) {
      var savedValue = el.value;
      el.value = "";
      el._baseScrollHeight = el.scrollHeight;
      el.value = savedValue;
    },
    mouseEnter: function () {
      this.toFirstPlan();
      if (
        (this.block.type === "subpage" ||
          this.block.type === "subpage-cover") &&
        this.block.content.ref.length > 0
      ) {
        this.store.highlightBlockByTitle(this.block.content.ref);
        this.store.highlightBlockById(this.block.id);
      }
    },
    mouseLeave: function () {
      this.toInitialPlan();
      if (
        (this.block.type === "subpage" ||
          this.block.type === "subpage-cover") &&
        this.block.content.ref.length > 0
      ) {
        this.store.unhighlightBlockByTitle(this.block.content.ref);
        this.store.unhighlightBlockById(this.block.id);
      }
    },
    duplicate: function (event) {
      console.log(event);
    },
    toFirstPlan: function () {
      this.isHover = true;
      this.$refs.block.style.zIndex = this.$root.maxZIndex + 1;
    },
    toInitialPlan: function () {
      this.isHover = false;
      this.$refs.block.style.zIndex = this.block.content.zindex;
    },
  },
};

export default Block;
