import Store from "../../store.js"
import Block from "./block.js"

const Blocks = {
    props: [
        'blocks', 'children', 'updateSignal', 'page'
    ],
    components: {
        'block': Block,
    },
    data: function() {
      return {
        store: Store
      }
    },
    computed: {
      isSelectionActive: function() {
        return this.store.state.blocks.localVersion.some(block => block.isSelected)
      }
    },
    template: `
        <section class="blocks dragscroll" :class="store.state.layout">
            <div 
              id="selection" 
              v-if="isSelectionActive"
            >
              <block 
                  v-for="(block, index) in blocks" 
                  v-if="block.isSelected && (store.state.layout !== 'full' || index === store.state.slideIndex)"
                  :block="block" 
                  :update-signal="updateSignal"
                  :page="page"
                  :key="block.id"
              ></block>
            </div>
            <block 
                v-for="(block, index) in blocks" 
                v-if="
                  (store.state.layout !== 'free' || !block.isSelected) 
                  && (store.state.layout !== 'full' || index === store.state.slideIndex) 
                  && !block.isHidden
                  && (store.state.isDesktop || block.content.isdesktoponly !== 'true')
                "
                :block="block" 
                :update-signal="updateSignal"
                :page="page"
                :key="block.id"
            ></block>
        </section>
    `
}

export default Blocks