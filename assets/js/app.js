import Blocks from "./components/blocks/blocks.js";
import Store from "./store.js";
import apparatus from "./components/apparatus/apparatus.js";
import contextMenuMixin from "./components/apparatus/mixins/context-menu-mixin.js";
import Colors from "./components/buttons/colors.js";

// polyfills
if (!String.prototype.matchAll) {
  String.prototype.matchAll = function (rx) {
    if (typeof rx === "string") rx = new RegExp(rx, "g"); // coerce a string to be a global regex
    rx = new RegExp(rx); // Clone the regex so we don't update the last index on the regex they pass us
    let cap = []; // the single capture
    let all = []; // all the captures (return this)
    while ((cap = rx.exec(this)) !== null) all.push(cap); // execute and add
    return all; // profit!
  };
}
if (!String.prototype.replaceAll) {
  String.prototype.replaceAll = function (str, newStr) {
    // If a regex pattern
    if (
      Object.prototype.toString.call(str).toLowerCase() === "[object regexp]"
    ) {
      return this.replace(str, newStr);
    }

    // If a string
    return this.replace(new RegExp(str, "g"), newStr);
  };
}

if (document.querySelector("#app")) {
  const app = new Vue({
    el: "#app",
    mixins: [contextMenuMixin],
    components: {
      apparatus: apparatus,
      blocks: Blocks,
      colors: Colors,
    },
    data: {
      rootUrl: rootUrl,
      store: Store,
      index: index,
      isLog: isLog,
      tags: tags,
      page: pageUrl,
      pageUri: pageUri,
      apiUrl: apiUrl,
      updateSignal: 0,
      maxZIndex: null,
      minZIndex: null,
      isEditMode: true,
      debug: true,
      palette: palette,
      pageColorsLabel: pageColorsLabel,
      breadcrumb: breadcrumb,
    },
    computed: {
      new: function () {
        return this.blocks.find((block) => block.isNew) || false;
      },
      highestBlock: function () {
        let highestBlock = undefined;
        this.blocks.forEach((block) => {
          if (highestBlock === undefined) {
            highestBlock = block;
          } else {
            if (block.content.zindex > highestBlock.content.zindex) {
              highestBlock = block;
            }
          }
        });
        if (highestBlock == undefined) return false;
        return highestBlock || false;
      },
      lowestBlock: function () {
        let lowestBlock = undefined;
        this.blocks.forEach((block) => {
          if (lowestBlock === undefined) {
            lowestBlock = block;
          } else {
            if (block.content.zindex < lowestBlock.content.zindex) {
              lowestBlock = block;
            }
          }
        });
        return lowestBlock || false;
      },
      isParentPage: function () {
        if (this.blocks.some((block) => block.type === "page")) {
          return true;
        } else {
          return false;
        }
      },
      nbrEditedBlocks: function () {
        let nbrEditedBlocks = [];
        nbrEditedBlocks = this.blocks.filter((block) => block.isEdited);
        return nbrEditedBlocks.length;
      },
      coverImageBlock: function () {
        return (
          this.blocks.find((block) => block.content.iscover == true) || false
        );
      },
      blocks: function () {
        if (this.store.state.filters.length === 0)
          return this.store.state.blocks.localVersion;

        return this.store.state.blocks.localVersion.filter((block) => {
          if (!block.content.tags) return false;
          const tags = block.content.tags.split(/[,\/]+/);
          return tags.some((tag) => {
            return this.store.state.filters.some((filter) => filter === tag);
          });
        });
      },
      pageColors: function () {
        return (
          this.palette.find((color) => color.label === this.pageColorsLabel) ||
          false
        );
      },
    },
    watch: {
      highestBlock: {
        handler: function (highestBlock) {
          if (highestBlock == undefined || highestBlock.content === undefined)
            return;
          this.maxZIndex = parseInt(highestBlock.content.zindex);
        },
        immediate: true,
      },
      lowestBlock: {
        handler: function (lowestBlock) {
          if (lowestBlock == undefined || lowestBlock.content === undefined)
            return;
          this.minZIndex = parseInt(lowestBlock.content.zindex);
        },
        immediate: true,
      },
      isEditMode: function (isEditMode) {
        if (isEditMode) {
          document.querySelector("html").classList.add("grid");
        } else {
          document.querySelector("html").classList.remove("grid");
        }
      },
      pageColors: function (pageColors) {
        document.querySelector("html").style.color = pageColors.text;
        document.querySelector("html").style.backgroundColor =
          pageColors.background;
      },
    },
    methods: {
      initiaOffset: function (selector) {
        const initialOffset = {
          x: 30,
          y: 30,
        };

        return initialOffset;
      },
      setInteract: function () {
        let position;
        const snapStep = 15;
        const initialOffset = this.initiaOffset(".blocks");
        interact(".block:not(.edit, .lock, .block--code)").resizable({
          edges: {
            top: false,
            left: false,
            bottom: false,
            right: true,
          },
          listeners: {
            start: (event) => {
              event.target.classList.add("front");
              event.target.classList.add("block--selected");
              event.target.classList.add("prevent-unselect");
              document.body.classList.add("unselectable");
            },
            move: (event) => {
              this.store.state.isUpToDate = false;
              let { x, y } = event.target.dataset;

              x = (parseFloat(x) || 0) + event.deltaRect.left;
              y = (parseFloat(y) || 0) + event.deltaRect.top;

              Object.assign(event.target.style, {
                width: `${event.rect.width}px`,
                height: `${event.rect.height}px`,
              });

              Object.assign(event.target.dataset, {
                x,
                y,
              });
            },
            end: (event) => {
              event.target.classList.remove("block--selected");
              setTimeout(() => {
                event.target.classList.remove("prevent-unselect");
              }, 500);

              setTimeout(() => {
                this.store.backupBlocks();
              }, 100);

              event.target.classList.remove("front");
              document.body.classList.remove("unselectable");
            },
          },
          modifiers: [
            interact.modifiers.snapSize({
              targets: [
                {
                  width: 100,
                },
                interact.snappers.grid({
                  width: snapStep,
                  height: snapStep,
                }),
              ],
            }),
          ],
        });
        interact(".block--code").resizable({
          edges: {
            top: false,
            left: false,
            bottom: true,
            right: true,
          },
          listeners: {
            start: (event) => {
              event.target.classList.add("front");
              event.target.classList.add("block--selected");
              event.target.classList.add("prevent-unselect");
              document.body.classList.add("unselectable");
            },
            move: (event) => {
              this.store.state.isUpToDate = false;
              let { x, y } = event.target.dataset;

              x = (parseFloat(x) || 0) + event.deltaRect.left;
              y = (parseFloat(y) || 0) + event.deltaRect.top;

              Object.assign(event.target.style, {
                width: `${event.rect.width}px`,
                height: `${event.rect.height}px`,
              });

              Object.assign(event.target.dataset, {
                x,
                y,
              });
            },
            end: (event) => {
              event.target.classList.remove("block--selected");
              setTimeout(() => {
                event.target.classList.remove("prevent-unselect");
              }, 500);

              setTimeout(() => {
                this.store.backupBlocks();
              }, 100);

              event.target.classList.remove("front");
              document.body.classList.remove("unselectable");
            },
          },
          modifiers: [
            interact.modifiers.snapSize({
              targets: [
                {
                  width: 100,
                },
                interact.snappers.grid({
                  width: snapStep,
                  height: snapStep,
                }),
              ],
            }),
          ],
        });
        interact(".block--image.selected")
          .draggable({
            autoScroll: true,
            listeners: {
              start(event) {
                event.target.classList.add("front");
                event.target.classList.add("block--selected");
                position = {
                  x: event.target.getBoundingClientRect().x - initialOffset.x,
                  y: event.target.getBoundingClientRect().y - initialOffset.y,
                };
              },
              move(event) {
                position.x += event.dx;
                position.y += event.dy;

                event.target.style.transform = `translate(${position.x}px, ${position.y}px)`;
              },
              end: (event) => {
                event.target.classList.remove("block--selected");
                setTimeout(() => {
                  this.store.backupBlocks();
                }, 100);
                event.target.classList.remove("front");
              },
            },

            modifiers: [
              interact.modifiers.snap({
                targets: [
                  interact.snappers.grid({
                    x: snapStep,
                    y: snapStep,
                  }),
                ],
              }),
            ],
          })
          .resizable({
            edges: {
              top: false,
              left: false,
              bottom: true,
              right: true,
            },
            listeners: {
              start: (event) => {
                document.body.classList.add("unselectable");
                event.target.classList.add("block--selected");
                event.target.classList.add("prevent-unselect");
              },
              move: (event) => {
                let { x, y } = event.target.dataset;

                x = (parseFloat(x) || 0) + event.deltaRect.left;
                y = (parseFloat(y) || 0) + event.deltaRect.top;

                Object.assign(event.target.style, {
                  width: `${event.rect.width}px`,
                  height: `${event.rect.height}px`,
                });

                Object.assign(event.target.dataset, {
                  x,
                  y,
                });
              },
              end: (event) => {
                this.store.backupBlocks();
                document.body.classList.add("unselectable");
                event.target.classList.remove("block--selected");
                setTimeout(() => {
                  event.target.classList.remove("prevent-unselect");
                }, 500);
              },
            },
            modifiers: [
              interact.modifiers.snapSize({
                targets: [
                  {
                    width: 100,
                  },
                  interact.snappers.grid({
                    width: snapStep,
                    height: snapStep,
                  }),
                ],
              }),
              interact.modifiers.aspectRatio({
                // make sure the width is always double the height
                ratio: "preserve",
              }),
            ],
          });
        interact(".free .block, #selection").draggable({
          ignoreFrom: "h1, h2, h3, h4, p",
          autoScroll: {
            container: window,
            distance: 100,
            interval: snapStep,
            speed: 10000,
          },
          listeners: {
            start: (event) => {
              if (event.target.id !== "selection")
                event.target.classList.add("block--selected");
              if (
                event.target.classList.contains("block--representative") &&
                event.target.querySelector(".block--representative__image-link")
              ) {
                event.target
                  .querySelector(".block--representative__image-link")
                  .classList.add("hidden");
              }
              if (event.target.id === "selection") {
                event.target.style.zIndex = 800;
              }
              position = {
                x:
                  event.target.getBoundingClientRect().x -
                  (initialOffset.x - window.scrollX),
                y:
                  event.target.getBoundingClientRect().y -
                  (initialOffset.y - window.scrollY),
              };

              event.target.classList.add("front");
              document.body.classList.add("unselectable");
            },
            move: (event) => {
              this.store.state.isUpToDate = false;
              position.x += event.dx;
              position.y += event.dy;

              position.x = Math.round(position.x / snapStep) * snapStep;
              position.y = Math.round(position.y / snapStep) * snapStep;

              event.target.style.transform = `translate(${position.x}px, ${position.y}px)`;
              this.setAppSize("move");
            },
            end: (event) => {
              event.target.classList.remove("block--selected");
              setTimeout(() => {
                this.store.unselectBlockById(event.target.dataset.id);
              }, 10);
              if (
                event.target.classList.contains("block--representative") &&
                event.target.querySelector(".block--representative__image-link")
              ) {
                event.target
                  .querySelector(".block--representative__image-link")
                  .classList.remove("hidden");
              }
              setTimeout(() => {
                this.store.backupBlocks();
              }, 100);

              this.setAppSize();

              event.target.classList.remove("front");
              document.body.classList.remove("unselectable");
            },
          },
          modifiers: [
            interact.modifiers.snap({
              targets: [
                interact.snappers.grid({
                  x: snapStep,
                  y: snapStep,
                }),
              ],
            }),
          ],
        });
      },
      addState: function () {
        this.blocks.forEach((block) => {
          block.isEdit = false;
          block.isEdited = false;
          block.isNew = false;
        });
      },
      add: function (block) {
        if (this.$root.debug) {
          console.log(this.$options.el + " add: ", block);
        }
        this.blocks.push(block);
      },
      getEditedBlocks: function () {
        const editedBlocks = this.blocks.filter((block) => !block.isNew);
        if (editedBlocks.length > 0) {
          return editedBlocks;
        } else {
          return false;
        }
        if (this.$root.debug) {
          console.log(
            this.$options._componentTag + " editedBlocks: ",
            editedBlocks
          );
        }
      },
      removeNewBlock: function () {
        if (
          !this.store.state.blocks.localVersion.some(
            (block) => block.content.isedit === true
          )
        ) {
          this.store.removeNewBlock();

          this.$emit("reset-options");
          if (this.debug) {
            console.log(this.$options.el + " removeNewBlock");
          }
        }
      },
      keysShortcut: function () {
        document.addEventListener("keydown", (event) => {
          if (event.ctrlKey || event.metaKey) {
            if (event.keyCode === 90) {
              this.store.undo();
            }
          }

          if (event.key === "Escape") {
            this.removeNewBlock();
          }

          if (this.store.state.layout === "full") {
            if (event.key === "ArrowLeft") {
              document
                .querySelector("#slide-nav__switch--prev")
                .classList.add("selected");
              setTimeout(() => {
                document
                  .querySelector("#slide-nav__switch--prev")
                  .classList.remove("selected");
              }, 100);
              if (this.store.state.slideIndex === 0) {
                this.store.state.slideIndex =
                  this.store.state.blocks.localVersion.length - 1;
              } else {
                this.store.state.slideIndex--;
              }
            } else if (event.key === "ArrowRight") {
              document
                .querySelector("#slide-nav__switch--next")
                .classList.add("selected");
              setTimeout(() => {
                document
                  .querySelector("#slide-nav__switch--next")
                  .classList.remove("selected");
              }, 100);
              if (
                this.store.state.slideIndex ===
                this.store.state.blocks.localVersion.length - 1
              ) {
                this.store.state.slideIndex = 0;
              } else {
                this.store.state.slideIndex++;
              }
            }
          }
        });
      },
      setPageColors: function () {
        document.querySelector("html").style.backgroundColor =
          this.pageColors.background;
        document.querySelector("html").style.color = this.pageColors.text;
      },
      hideContextMenu: function (e) {
        document.querySelector("#context-menu").classList.add("hidden");
        this.clearSelection(e);
      },
      clearSelection: function (e) {
        if (e.target.id !== "app") return;
        let sel;
        if ((sel = document.selection) && sel.empty) {
          sel.empty();
        } else {
          if (window.getSelection) {
            window.getSelection().removeAllRanges();
          }
          const activeEl = document.activeElement;
          if (activeEl) {
            let tagName = activeEl.nodeName.toLowerCase();
            if (
              tagName == "textarea" ||
              (tagName == "input" && activeEl.type == "text")
            ) {
              // Collapse the selection to the end
              activeEl.selectionStart = activeEl.selectionEnd;
            }
          }
        }
      },
      resetScroll: function () {
        window.scrollTo(0, 0);
      },
      initBackups: function () {
        let allBackups = JSON.parse(localStorage.getItem("backups"))
          ? JSON.parse(localStorage.getItem("backups"))
          : {};
        // const pageBackups = [blocks]
        const pageBackups = allBackups[this.page]
          ? allBackups[this.page]
          : [blocks];
        allBackups[this.page] = pageBackups;
        localStorage.setItem("backups", JSON.stringify(allBackups));
      },
      getPageLastBackup: function () {
        const pageBackups = JSON.parse(localStorage.getItem("backups"))[
          this.page
        ];
        const lastBackup = pageBackups[pageBackups.length - 1];
        return lastBackup;
      },
      enableDragToScroll: function () {
        const ele = document.getElementById("app");
        ele.style.cursor = "grab";

        let pos = { top: 0, left: 0, x: 0, y: 0 };

        const mouseDownHandler = function (e) {
          const isVerticalMode = e.target.classList.contains("vertical");
          if (!e.target.classList.contains("dragscroll")) return;
          document.querySelector("html").style.scrollBehavior = "auto";

          ele.style.cursor = "grabbing";
          ele.style.userSelect = "none";

          pos = {
            left: window.scrollX,
            top: window.scrollY,
            // Get the current mouse position
            x: e.clientX,
            y: e.clientY,
          };

          document.addEventListener("mousemove", mouseMoveHandler);
          document.addEventListener("mouseup", mouseUpHandler);
        };

        const mouseMoveHandler = function (e) {
          // How far the mouse has been moved
          const dx = e.clientX - pos.x;
          const dy = e.clientY - pos.y;

          // Scroll the element
          window.scrollTo(pos.left - dx, pos.top - dy);
        };

        const mouseUpHandler = function () {
          ele.style.cursor = "grab";
          ele.style.removeProperty("user-select");

          document.removeEventListener("mousemove", mouseMoveHandler);
          document.removeEventListener("mouseup", mouseUpHandler);

          document.querySelector("html").removeAttribute("style");
        };

        // Attach the handler
        ele.addEventListener("mousedown", mouseDownHandler);
      },
      setAppSize: function (mode = "end") {
        const app = document.querySelector("#app");

        const margin = 400;
        const width =
          this.store.getBlockRightPosition(this.store.getRightMostBlock()) +
          margin +
          "px";
        const height =
          this.store.getBlockBottomPosition(this.store.getLowestBlock()) +
          margin +
          "px";

        const hasToReduce =
          app.style.width > width || app.style.height > height;
        if (mode === "end" && hasToReduce) {
          app.style.transition =
            "width .5s ease-in-out, height .5s ease-in-out";
        }
        if (mode === "move") {
          if (app.style.width < width) app.style.width = width;
          if (app.style.height < height) app.style.height = height;
        } else {
          app.style.width = width;
          app.style.height = height;
        }

        setTimeout(() => {
          app.style.transition = "";
        }, 600);
      },
      initAppSize: function () {
        document.querySelector("#app").style.transition =
          "width .5s ease-in-out, height .5s ease-in-out";
        setTimeout(() => {
          if (this.isLog) {
            this.setInteract();
          }
          this.setAppSize();
          setTimeout(() => {
            this.setAppSize();
            setTimeout(() => {
              this.setAppSize();
              setTimeout(() => {
                this.setAppSize();
                if (app.style) app.style.transition = "";
              }, 500);
            }, 500);
          }, 100);
        }, 100);
      },
      enableLeaveDisclaimer: function () {
        if (this.isLog) {
          window.addEventListener("beforeunload", (event) => {
            if (this.store.state.isUpToDate) return;
            event.returnValue =
              "Certaines modifications ne sont pas enregistrées. Êtes-vous sûr·e de vouloir quitter la page ?";
            confirm(
              "Certaines modifications ne sont pas enregistrées. Êtes-vous sûr·e de vouloir quitter la page ?"
            );
          });
        }
      },
      enableClearSelection: function () {
        document.querySelector("#app").addEventListener("click", (e) => {
          if (e.target.id === "app" || e.target.id === "selection")
            this.store.clearSelection();
        });
      },
      enableReloadOnResize: function () {
        window.addEventListener("resize", () => {
          if (window.innerWidth < 850) {
            if (this.store.state.isDesktop) {
              location.reload();
            }
          } else {
            if (this.store.state.isMobile) {
              location.reload();
            }
          }
        });
      },
    },
    created: function () {
      this.store.setLayout(layout);
      this.store.setOriginalLayout(layout);
    },
    mounted: function () {
      const variables = [
        {
          key: "isLog",
          value: isLog,
        },
        {
          key: "isOpenSpace",
          value: isOpenSpace,
        },
      ];

      // this.enableReloadOnResize();
      this.store.storeVariables(variables);
      this.enableDragToScroll();
      // this.initBackups();
      this.store.storePageName(this.page);
      // this.store.setBlocks(blocks)
      this.store.setOriginalBlocks(blocks);

      // if (this.isLog) {
      this.store.setBlocks(blocks);
      // } else {
      //   this.store.setBlocks(this.getPageLastBackup());
      // }
      this.store.proceedServerUpdates();
      this.initAppSize();
      this.keysShortcut();
      this.enableLeaveDisclaimer();
      this.enableClearSelection();
    },
  });
}
