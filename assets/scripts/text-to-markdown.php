<?php
function replaceStringInFiles($dir, $search, $replace)
{
    // Récupérer la liste des fichiers et dossiers dans le répertoire actuel
    $files = scandir($dir);

    // Parcourir les fichiers et dossiers
    foreach ($files as $file) {
        // Ignorer les fichiers et dossiers spéciaux
        if ($file === '.' || $file === '..') {
            continue;
        }

        $path = $dir . '/' . $file;

        // Vérifier si le chemin est un dossier
        if (is_dir($path)) {
            // Appeler récursivement la fonction pour le sous-dossier
            replaceStringInFiles($path, $search, $replace);
        } else {
            // Vérifier si le fichier est un fichier .txt
            if (pathinfo($path, PATHINFO_EXTENSION) === 'txt') {
                // Lire le contenu du fichier
                $content = file_get_contents($path);

                // Remplacer la chaîne de caractères
                $newContent = str_replace($search, $replace, $content);

                // Écrire le contenu modifié dans le fichier
                file_put_contents($path, $newContent);
            }
        }
    }
}

// Spécifier le répertoire racine du projet
$rootDir = '../../content';

// Chaîne de caractères à rechercher
$searchString = '"type":"text"';

// Chaîne de caractères de remplacement
$replaceString = '"type":"markdown"';

// Appeler la fonction pour remplacer les chaînes de caractères
replaceStringInFiles($rootDir, $searchString, $replaceString);

echo "Le remplacement des chaînes de caractères a été effectué avec succès !";
?>
