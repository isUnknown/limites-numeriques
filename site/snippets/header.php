<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php snippet('meta') ?>
    <link rel="icon" type="image/x-icon" href="<?= url('assets') ?>/images/icons/favicon.svg">
    <meta name="robots" content="index, follow"> 
    <link rel="canonical" href="<?= $page->url() ?>">
    
    <!--========== INTERACT ==========-->
    <?php if ($kirby->user()): ?>
      <script src="<?= url('assets') ?>/js/libs/interact.min.js"></script>
      <script src="<?= url('assets') ?>/js/libs/vue-interactjs.umd.js"></script>
    <?php endif ?>
    
    <!--========== VUE ==========-->
    <script src="<?= url('assets') ?>/js/libs/vue.js"></script>

    <!--========== DEVELOPMENT : RAW ==========-->
    <!-- <script src="<?= url('assets') ?>/js/app.js" type="module" defer></script>
    <link rel="stylesheet" href="<?= url('assets') ?>/css/style.css?version-cache-prevent<?= rand(0, 1000)?>"> -->

    <!--========== PRODUCTION : BUNDLES ==========-->
    <script src="<?= url('assets') ?>/dist/app.bundle.js" type="module" defer></script>
    <link rel="stylesheet" href="<?= url('assets') ?>/dist/style.min.css?version-cache-prevent<?= rand(0, 1000)?>">
    
    <!--========== MARKEDJS (MARKDOWN PARSER) ==========-->
    <script src="<?= url('assets') ?>/js/libs/marked.min.js"></script>

    <!--========== MATOMO ==========-->
    <script>
      var _paq = window._paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="//praticable.fr/matomo/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '2']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>

    <!-- Visites-->
    <script>
      var _paq = window._paq = window._paq || [];
      /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="//visites.limitesnumeriques.fr/";
        _paq.push(['setTrackerUrl', u+'visites.php']);
        _paq.push(['setSiteId', '1']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.async=true; g.src=u+'visites.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
<!-- End Visites Code -->


    <!--========== FONTS ==========-->
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/iAWriterMonoV.ttf" type="font/ttf" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/iAWriterQuattroV-Italic.ttf" type="font/ttf" crossorigin="anonymous">
    <link rel="preload" as="font" href="<?= url('assets') ?>/fonts/iAWriterQuattroV.ttf" type="font/ttf" crossorigin="anonymous">

    <!--========== PRELOAD ==========-->
    <link rel="preload" href="<?= url('assets') ?>/images/icons/facebook-hover.svg">
    <link rel="preload" href="<?= url('assets') ?>/images/icons/twitter-hover.svg">
    <link rel="preload" href="<?= url('assets') ?>/images/icons/instagram-hover.svg">
    <link rel="preload" href="<?= url('assets') ?>/images/icons/mastodon-hover.svg">
    <link rel="preload" href="<?= url('assets') ?>/images/icons/linkedin-hover.svg">

    <style>
      :root {
        --color-main: <?php e($page->color()->isNotEmpty(), $page->color(), '#15796A') ?>
      }
    </style>
</head>
<body>
    <?php snippet('svg') ?>