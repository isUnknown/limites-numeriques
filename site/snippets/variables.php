<script>
  const csrf = '<?= csrf() ?>';
  const isLog = <?php e($kirby->user(), 'true', 'false') ?>;
  const blocks = <?= json_encode($blocks) ?>;
  const apiUrl = '<?= $page->apiUrl() ?>';
  const pageUrl = '<?= addslashes($page->title()) ?>';
  const pageUri = '<?= addslashes($page->uri()) ?>';
  const palette = <?= json_encode($site->palette()->toStructure()->toArray()) ?>;
  const pageName = '<?= addslashes($page->title()) ?>';
  const pageColorsLabel = '<?php e($page->colors()->isNotEmpty(), $page->colors(), 'gris') ?>';
  const rootUrl = '<?= $site->url() ?>';
  const index = <?= $kirby->collection('index') ?>;
  const tags = <?= $kirby->collection('tags') ?>;
  const layout = '<?= $page->layout() ?>';
  const isOpenSpace = <?php e($page->isOpenSpace()->isEmpty(), 'false', $page->isOpenSpace()->value()) ?>;
  <?php
    $breadcrumb = [];
    foreach($site->breadcrumb() as $crumb) {
      $breadcrumb[] = $crumb->title()->lower()->value();
    }
  ?>
  const breadcrumb = <?= json_encode($breadcrumb) ?>;
</script>