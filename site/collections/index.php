<?php

return function ($site) {
  $index = [];
  
  if ($site->hasChildren()) {
    foreach ($site->children()->listed()->filterBy('template', 'space') as $child) {
      if (!$child->isErrorPage()) {
        $index[] = getRecursiveTitleAndUrl($child, 'space');
      }
    }
  }

  return json_encode($index);
};