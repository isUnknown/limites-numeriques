<?php

return [
  'pattern' => ['(:all)save.json'],
  'method' => 'POST',
  'action' => function() {
      // GET BLOCK DATA FROM BODY REQUEST
      $jsonRequest = file_get_contents("php://input");
      $request = json_decode($jsonRequest);

      // FIND PAGE
      $site = site();
      $page = $site->index(true)->findBy('uri', $request->pageUri);

      // ACTIVE SUPERUSER FOR DATA MANIPULATIONS
      $kirby = kirby();
      $kirby->impersonate('kirby');

      // CLEAR COMPOSITION IF NO BLOCKS
      if (count($request->blocks) === 0) {
          $page->update([
              'composition' => json_encode([]),
              'colors' => $request->colors
          ]);
          return json_encode("Composition cleared");
      } else {
        $previousBlocks = $page->composition()->toBlocks();
          $newBlocks = [];

          $coverThumb = '';
          $coverThumbTransform = $page->coverThumbTransform();
          $pageThumbTransform = $page->pageThumbTransform();

          foreach ($request->blocks as $incomingBlock) {
            // PROCESS NEW BLOCK
            if (property_exists($incomingBlock, 'isNew') && $incomingBlock->isNew == 'true') {
                //================================================================ FILE
                if ($incomingBlock->type === 'image') {
                    $fileName = $incomingBlock->content->image[0]->filename;
                    // CREATE TEMP FILE IN ASSETS FOLDER
                    $path = './assets/files/' . $fileName;
                    $file = fopen($path, "wb");
                    fwrite($file, base64_decode($incomingBlock->b64));
                    fclose($file);

                    if ($page->files()->has(strtolower($request->page). '/' . $fileName)) {
                        unlink($path);
                        return json_encode('Files already exists');
                        die();
                    }

                    // CREATE FILE IN PAGE FOLDER
                    $kirbyFile = null;
                    $kirbyFile = $page->createFile([
                        'source' => $path,
                        'filename' => $fileName
                    ]);

                    // CREATE BLOCK 
                    $preparedBlock = new Kirby\Cms\Block([
                        'content' => [
                            'location' => 'kirby',
                            'image' => $kirbyFile->fileName(),
                            'width' => $incomingBlock->content->width,
                            'height' => $incomingBlock->content->height,
                            'transform' => $incomingBlock->content->transform,
                            'zindex' => (int)$incomingBlock->content->zindex,
                            'isCover' => $incomingBlock->content->iscover,
                            'caption' => property_exists($incomingBlock->content, 'caption') ? $incomingBlock->content->caption : null
                        ],
                        'type' => 'image'
                    ]);
                    if ($incomingBlock->content->iscover == 'true') {                                
                      $coverThumb = (string)$kirbyFile->mediaUrl();
                    }

                    $newBlocks[] = $preparedBlock->toArray();
                    unlink($path);
                }
                //================================================================ TEXT
                elseif ($incomingBlock->type === 'markdown' || $incomingBlock->type === 'text') {
                    // CREATE BLOCK 
                    $preparedBlock = new Kirby\Cms\Block([
                        'content' => [
                            'text' => $incomingBlock->content->text,
                            'width' => $incomingBlock->content->width,
                            'height' => $incomingBlock->content->height,
                            'transform' => $incomingBlock->content->transform,
                            'zindex' => (int)$incomingBlock->content->zindex,
                            'backgroundcolor' => '#FFF',
                            'textcolor' => '#000',
                            'isedit' => false,
                            'refs' => $incomingBlock->content->refs
                        ],
                        'isHidden' => false,
                        'id' => $incomingBlock->id,
                        'type' => 'markdown'
                    ]);
                    
                    $newBlocks[] = $preparedBlock->toArray();
                }

                //================================================================ TEXT
                elseif ($incomingBlock->type === 'code') {
                  // CREATE BLOCK 
                  $preparedBlock = new Kirby\Cms\Block([
                      'content' => [
                          'code' => $incomingBlock->content->code,
                          'width' => $incomingBlock->content->width,
                          'height' => $incomingBlock->content->height,
                          'transform' => $incomingBlock->content->transform,
                          'zindex' => (int)$incomingBlock->content->zindex,
                          'backgroundcolor' => $incomingBlock->content->backgroundcolor,
                          'textcolor' => $incomingBlock->content->textcolor
                      ],
                      'id' => $incomingBlock->id,
                      'type' => 'code'
                  ]);
                  
                  $newBlocks[] = $preparedBlock->toArray();
              }

            } 
            // PROCESS EDITED BLOCK
            else {
                foreach ($previousBlocks as $previousBlock) {
                    // UPDATE EXISTING BLOCKS
                    if ($previousBlock->id() === $incomingBlock->id) {
                      $newBlocks[] = $incomingBlock;
                      if ($incomingBlock->type === 'image' && $incomingBlock->content->iscover == 'true') {
                        $coverThumb = (string)$previousBlock->image()->toFile()->mediaUrl();
                      }
                    }
                }
            }
          }

          $flatNewBlocks = [];
          $flatNewBlocks = array_values($newBlocks);

          $pageThumbTransform = $page->pageThumbTransform();
          $newPage = $page->update([
              'composition' => json_encode($flatNewBlocks),
              'coverThumb' => $coverThumb,
              'coverThumbTransform' => $coverThumbTransform,
              'pageThumbTransform' => $pageThumbTransform,
              'colors' => isset($request->colors) ? $request->colors : null
          ]);

          return json_encode([
              'new' => isset($preparedBlock) ? $preparedBlock->toArray(): false,
              'blocks' => $newPage->composition()->toBlocks()->toArray()
          ]);
      }
  }
];