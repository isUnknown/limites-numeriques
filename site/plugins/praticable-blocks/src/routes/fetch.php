<?php

return [
  "pattern" => ['(:any)/fetch.json', '/fetch.json'],
  "method" => "POST",
  "action" => function() {
    $json = file_get_contents("php://input");
    $data = json_decode($json);

    $site = site();
    $page = $site->index()->findBy('slug', $data->pageTitle);

    $connections = $page->connections()->toStructure();

    foreach ($connections as $connection) {
      // FIND CORRESPONDING API
      $targetApi = preg_replace('/\s-(.*)/', '', (string)$connection->api());
      $api = $site->apis()->toStructure()->findBy('name', $targetApi);

      if ($api->tool() == 'notion') {
        $url = 'https://api.notion.com/v1/blocks/' . (string)$connection->id() . '/children?page_size=100';

        $headers = [
          'Notion-Version: ' . (string)$api->version(),
          'Authorization: Bearer ' . (string)$api->key()
        ];

        //QUERY API
        $curl = curl_init();
        $json = getNotionBlocks($curl, $url, $headers);
        curl_close($curl);

        createJSONFile($page, (string)$api->id(), $json);

        $response = json_decode($json);
        $incomingBlocks = $response->results;
        
        $preparedBlocks = prepareBlocksRecursively($incomingBlocks, $api, $page, (string)$connection->target());

        $page->update([
          'composition' => json_encode($preparedBlocks)
        ]);

        return json_encode($preparedBlocks);
      }
    }
  }
];