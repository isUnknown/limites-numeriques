<?php
function prepareBlocksRecursively($incomingBlocks, $api, $page, $target) {
  $preparedBlocks = [];
  foreach ($incomingBlocks as $incomingBlock) {
    $preparedBlock = prepareKBlockFromNotionBlock($incomingBlock, $api, $page, $target);
    
    if ($preparedBlock) {
      $preparedBlocks[] = $preparedBlock->toArray();
    }

    // GET COLUMN CHILDREN BLOCKS
    if ($incomingBlock->type === 'column_list') {                  
        $url = 'https://api.notion.com/v1/blocks/' . $incomingBlock->id . '/children?page_size=100';
  
        $headers = [
          'Notion-Version: ' . (string)$api->version(),
          'Authorization: Bearer ' . (string)$api->key()
        ];
  
        //QUERY API
        $curl = curl_init();
        $json = getNotionBlocks($curl, $url, $headers);
        curl_close($curl);
  
        // createJSONFile($page, $incomingBlock->id, $json);
  
        $response = json_decode($json);
        $incomingBlocks = $response->results;

        foreach ($incomingBlocks as $incomingBlock) {
          $url = 'https://api.notion.com/v1/blocks/' . $incomingBlock->id . '/children?page_size=100';
  
          $headers = [
            'Notion-Version: ' . (string)$api->version(),
            'Authorization: Bearer ' . (string)$api->key()
          ];
    
          //QUERY API
          $curl = curl_init();
          $json = getNotionBlocks($curl, $url, $headers);
          curl_close($curl);
    
          createJSONFile($page, $incomingBlock->id, $json);
    
          $response = json_decode($json);
          $incomingBlocks = $response->results;

          foreach ($incomingBlocks as $incomingBlock) {
            $preparedBlock = prepareKBlockFromNotionBlock($incomingBlock, $api, $page, $target);
    
            if ($preparedBlock) {
              $preparedBlocks[] = $preparedBlock->toArray();
            }
          }
        }
    }
  }
  return $preparedBlocks;
}