<?php

$kirby = kirby();
if (!is_dir($kirby->root('assets') . '/static-data/' . $page->slug())) {
  mkdir($kirby->root('assets') . '/static-data/' . $page->slug());
}