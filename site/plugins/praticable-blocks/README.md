# Praticable blocks

This plugin store json specifically formated blocks to Kirby blocks.

## Installation

### Download

Download and copy this repository to `/site/plugins/praticable-blocks`.

### Git submodule

```
git submodule add git@framagit.org:praticable/praticable-blocks.git site/plugins/praticable-blocks
```

<!-- ### Composer

```
composer require adrienpayet/praticable-blocks
``` -->

<!-- ## Setup

_Additional instructions on how to configure the plugin (e.g. blueprint setup, config options, etc.)_ -->

## Options

_Documentation in progress_

The plugin supports image and text blocks.

<!-- ## Development

_Add instructions on how to help working on the plugin (e.g. npm setup, Composer dev dependencies, etc.)_ -->

## License

MIT

## Credits

- [Adrien Payet](https://framagit.org/isUnknown) for [Praticable](https://framagit.org/praticable)
